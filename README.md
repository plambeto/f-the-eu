# f the eu

A very simple greasemonkey script, which removes cookie consent forms (thank you, EU!) and sometimes other annoying dialogs around the web

# FAQ

#### Q: Why is site X still annoying? 
A: Because I've never seen it. Contact me.

#### Q: Does it click "ok" or does it just hide the annoying dialog? 
 A: It only hides stuff, and in special cases (e.g. trud.bg) it adds additional augmentation.


#### Q: What more does it do to trud.bg? 
A: It allows selecting and copying. Check their terms and/or agreement though and use at your own risk! 


### Q: How can I submit new sites to be de-EUshat on? 
A: Open an issue here. If you know me personally - you can also contact me directly elsewhere. 


### A: To anything else that may arise - check out the previous answer. 

