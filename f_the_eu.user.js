// ==UserScript==
// @name     F the EU
// @description Fuck the retarded EU rules, remove cookie consent and other similar stuff
// @version  0.3.1
// @grant    none
// @include  *
// ==/UserScript==

// Changelog:
// version 0.3.1: some sites added, changelog populated a bit more
// version 0.3: a lot of stuff, but mostly added new annoying things to remove
// version 0.2: rewritten in native js
// version 0.1: initial concept, written in jQuery

// complete rewrite in native js
function when_ready () {
  // around 4 seconds: 
  var repeats = 100;
  var timeout = 500;
  
  var global_selectors = [];
  
  // youtube adblock nagger:
  global_selectors.push(".ytd-app");
  
  // found at bazar.bg: 
  global_selectors.push(".popUP-container");
  global_selectors.push(".popUP");
  
  // found at techradar.com: 
  global_selectors.push(".qc-cmp-ui-container");
  
  // found at knowyourmeme.com: 
  global_selectors.push(".blocker");
  global_selectors.push(".qc-cmp-showing");
  
  // found at dnevnik.bg: 
  global_selectors.push("#cookies-banner");
  
  // found at riptutorial.com:
  global_selectors.push("#pgdg-gdpr");
  
  // found at ultimate-guitar.com: 
  global_selectors.push("._2aXaV");
  
  // found at iconomist.bg: 
  global_selectors.push("#private_info");
  
  // found at offensive-security.com: 
  global_selectors.push("#CybotCookiebotDialog");
  
  // found at analog.com: 
  global_selectors.push("#cookie-consent-container");
  global_selectors.push(".modal-backdrop");
  
  // found at trud.bg:
  global_selectors.push("#cookie-notice");
  
  // found at techotopia.com: 
  global_selectors.push("#ez-cookie-dialog-wrapper");
  
  // found at oracle.com: 
  global_selectors.push(".truste_overlay");
  global_selectors.push(".truste_box_overlay");
  
  // found at download.cnet.com: 
  global_selectors.push("#onetrust-consent-sdk");
  
  // found at web.dev: 
  global_selectors.push("web-snackbar-container");
  
  // found at tutorialspoint.com: 
  global_selectors.push("#cmpbox");
  
  // found at politifact.com: 
  global_selectors.push("#gdpr-modal-html");
  
  // found at codeproject.com: 
  global_selectors.push(".cc-window");
  
  // found at sarpi.fatdog.eu: 
  global_selectors.push(".cc_banner-wrapper");
  
  // found at sourceforge.net: 
  global_selectors.push("#cmpbox2");
  
  // found at europa.eu: 
  global_selectors.push("#cookie-consent-banner");
  
  // found at slate.com: 
  global_selectors.push(".as-oil");
  
  // found at theguardian.com: 
  global_selectors.push(".site-message--banner");
  
  // found at forums.freebsd.org: 
  global_selectors.push(".u-bottomFixer");
  
  // found at poftut.com: 
  global_selectors.push(".app_gdpr--2k2uB");
  
  // found at segabg.com: 
  global_selectors.push(".app_cmpAppGdpr--22TAr");
  
  // found at bbc.com: 
  global_selectors.push("#cookiePrompt");
  
  // found at omictools.com: 
  global_selectors.push("#cookie-policy-intro-dialog");

  // found at blog.ptsecurity.com:
  global_selectors.push("#cookieChoiceInfo");
  
  // found at vbox7.com:
  global_selectors.push("#onesignal-popover-container");
  global_selectors.push("#cmp-faktor-io-parent");
  
  // found at w3schools:
  global_selectors.push("#snigel-cmp-framework");

  // found at geeksforgeeks.com:
  global_selectors.push(".fc-consent-root");

  // found at pythoncentral.io:
  global_selectors.push("#gdpr-consent-tool-wrapper");
  
  // found at www.econt.com:
  global_selectors.push(".gdpr-modal");
  
  // found at www.forbes.com:
  global_selectors.push("#consent_blackbar");
  
  // found at www.forbes.com:
  global_selectors.push(".js-consent-banner");
  
  // found at explorer.zen-solutions.io:
  global_selectors.push("#iubenda-cs-banner");
  
  // found at www.3dsourced.com:
  global_selectors.push('[data-name="mediavine-gdpr-cmp"]');
  
  // found at cenobite.fandom.com:
  global_selectors.push('[data-tracking-opt-in-overlay="true"]');
  
  // found at buyzero.de:
  global_selectors.push('.overlay_bc_banner');
  
  // found at warlord0blog.wordpress.com:
  global_selectors.push('#cmp-app-container');
  
  // found at opennebula.io:
  global_selectors.push('#hs-eu-cookie-confirmation');
  
  // found at audeze.com:
  global_selectors.push('.csm-cookie-consent');

  
  // found at netplan.io:
  global_selectors.push('.cookie-policy');

  // found at getlabsdone.com:
  global_selectors.push(".EmtQcfKIUGIP-bg");
  global_selectors.push("#NFAacntaalBU");
  global_selectors.push("#NFAacntaalBU");
  global_selectors.push("#NFAacntaalBU");
  // continued below with an ugly workaround
  
  // non cookie consent stuff: 
  // found at youtube, the propaganda "clarify-box": 
  global_selectors.push("#clarify-box");
  
  function the_one_that_waits (the_one_that_waits_selectors) {
    // generic: 
    document.querySelector("body").style.overflow = "visible";
    
    if (repeats > 0) {
      the_one_that_waits_selectors.forEach(function (selector, index) {
        var element = document.querySelector(selector);
        //console.log ("Fuck the EU!!! Current selector that is being tested for is " + selector + "");
        if (element !== null) {
          element.style.opacity = "0";
          element.style.visibility = "hidden";
          //element.style.display = "none !important";
          element.style.setProperty("display", "none", "important");
          the_one_that_waits_selectors.splice(the_one_that_waits_selectors.indexOf(selector), 1);
          console.log ("Fuck the EU!!! Current selector is " + selector + " and it was exterminated!");
        }
      });
      setTimeout(the_one_that_waits, timeout, the_one_that_waits_selectors);
    }
    repeats = repeats - 1;
  }
  setTimeout(the_one_that_waits, timeout, global_selectors);
  
  // ugly workaround for getlabsdone.com:
  var getlabsdone_selectors = [];
  getlabsdone_selectors.push(".EmtQcfKIUGIP-bg");
  getlabsdone_selectors.push("#NFAacntaalBU");
  getlabsdone_selectors.push("#NFAacntaalBU");
  getlabsdone_selectors.push("#NFAacntaalBU");
  setTimeout(the_one_that_waits, 10000, getlabsdone_selectors);
  
  function site_specific_hider () {
    //trud copyright bullshit
    setTimeout(function () {
      document.body.onselectstart = function() {
       return true;
      }
      document.onselectstart = function() {
       return true;
      }
      document.onmousedown = function() {
       return true;
      }
    }, timeout);
    function check_for_event_handlers_and_overwrite_them(eventHandler) {
      if (typeof eventHandler == "function") {
        console.log(eventHandler + " is a function, so overwriting it");
        event = function() {
          return true;
        }
      } else {
        console.log(eventHandler + " is not a function yet");
        setTimeout(check_for_event_handlers_and_overwrite_them, timeout, eventHandler);
      }
    }
    if (window.location.href.startsWith("https://trud.bg") || window.location.href.startsWith("http://trud.bg")) {
      setTimeout(check_for_event_handlers_and_overwrite_them, timeout, document.body.onselectstart);
      setTimeout(check_for_event_handlers_and_overwrite_them, timeout, document.onselectstart);
      setTimeout(check_for_event_handlers_and_overwrite_them, timeout, document.onmousedown);
    }
  }
  setTimeout(site_specific_hider, timeout);
}

if (document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)) {
  when_ready();
} else {
  document.addEventListener("DOMContentLoaded", when_ready);
}
